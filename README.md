# EPP

Converts a raw CPP file to ***E***.

## Usage

```bash
$ epp input.cpp output.cpp
```

## Compilation

```bash
$ g++ ./epp.cpp -std=c++17 -o epp
```

## Hi

This is Payton writing a markdown file for Verve. I also made clockwork&libclock. Verve used it. So yes.


## recent updates

Complete overhaul of how the `Utills::split()` function works, aswell as optimized the amount of memory
new E++ files use.