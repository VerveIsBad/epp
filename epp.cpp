#include <iostream>
#include <fstream>
#include <unordered_map>
#include <vector>
#include <algorithm>



namespace Utills {

    // colors used by Utills::out()
    const std::string colorRed = "\033[1;31m";
    const std::string colorGreen = "\033[1;32m";
    const std::string resetColor = "\033[1m";

    // custom sort function used to sort the tokens vector in parse()
    struct Comparator {
        bool operator()(std::pair<std::string, int> a, std::pair<std::string, int> b) { return a.second > b.second; };
    };

    // used for pretty output
    void out(std::string msg, bool err = false, bool endLine = true, bool arrow = true) {
        std::cout << ( err ? colorRed : colorGreen ) << ( arrow ? "-> " : "" ) << msg << resetColor;
        if (endLine) std::cout << std::endl;
    }

    // used to search a vector of pairs for specific key.
    bool findKeyInPairs(std::vector<std::pair<std::string, int>> &v, std::string &k) {
        for (auto &p : v) {
            if (p.first == k) return true;
        }

        return false;
    }

    // returns index of matching pair inside of a vector
    int indexOfPairByKey(std::vector<std::pair<std::string, int>> &v, std::string &k) {
        for (int i = 0; i < v.size(); i++) {
            if (v[i].first == k) return i;
        }
        return -1;
    }

    // custom split function.
    // splits at whater the splitAt char is
    // also splits at specific special charaters
    std::vector<std::string> split(std::string s, char splitAt, bool ignoreStrings = true) {
        // empty string
        if (s.empty()) return {};

        std::string buffer = "";
        std::vector<char> specialChars = {',', '(', ')', '<','>', '{', '}', '[',']', ';'};
        std::vector<std::string> doubleSpecialChar = {"<<", ">>", "==", "!=", "<=", ">=", "->"};
        std::vector<std::string> words;

        char lastSpecialChar = 0; // used to check for double special characters

        bool hitDub             = false;
        bool hitSingle          = false;
        bool specialChar        = false;
        bool hashLine           = false;

        if (s.at(0) == '#') {
            return std::vector<std::string>({s});
        }

        // step one, split at spaces
        for (char token : s) {

            if (ignoreStrings) {
                if (token == '"') hitDub = !hitDub;
                if (token == '\'') hitSingle = !hitSingle;
            }

            if (hashLine == false) {

                // not inside string
                if (hitDub == false && hitSingle == false) {

                    if (token == splitAt) {
                        if (buffer != "") words.push_back(buffer);

                        // if we hit a space and a special char appeared before
                        // then we know its not a double special char
                        // so we can just push it back
                        // and mark last as empty
                        if (lastSpecialChar != 0) words.push_back(std::string(1, lastSpecialChar));
                        lastSpecialChar = 0;

                        buffer = "";
                        continue;
                    }

                    // check if the last + token is inside of the double special char vector
                    // if so, combine the two and push the result back
                    if (lastSpecialChar != 0) {

                        if (std::find(doubleSpecialChar.begin(), doubleSpecialChar.end(), std::string(1, lastSpecialChar) + std::string(1, token)) != doubleSpecialChar.end()) {
                            words.push_back(std::string(1, lastSpecialChar) + std::string(1, token));
                            buffer = "";
                            lastSpecialChar = 0;
                            continue;
                        } else { // if not a specialDoubleChar, push it back
                            words.push_back(std::string(1, lastSpecialChar));
                            lastSpecialChar = 0;
                        }

                    }

                    // check if token is in the special char vector
                    if (std::find(specialChars.begin(), specialChars.end(), token) != specialChars.end()) {
                        if (buffer != "") words.push_back(buffer);
                        buffer = "";
                        lastSpecialChar = token;
                        continue;
                    }

                } else if ((hitDub == true || hitSingle == true) && lastSpecialChar != 0) {
                    // for special characters that occure before a string
                    // this is required because of how I check for double special chars
                    words.push_back(std::string(1, lastSpecialChar));
                    lastSpecialChar = 0;
                }
            }

            buffer += token;

        }

        // push back lastSpecialChar word ( if there wasnt a space after it )
        if (buffer != "") words.push_back(buffer);
        if (lastSpecialChar != 0) words.push_back(std::string(1, lastSpecialChar));
        return words;

    }

};

// parse a c++ file and turn it into E's
void parse(std::string input, std::string output, bool newLines = false, bool verbose = false) {


    std::ifstream inputFile(input);
    std::ofstream outputFile(output);

    std::string line;
    std::string currE = "e";

    std::unordered_map<std::string, std::string> mapping;

    std::vector<std::pair<std::string, int>> tokenCount;

    std::vector<std::string> tokens;
    std::vector<std::string> includesAndDefines;

    bool hitHash        = false;
    bool hitMulti       = false;
    bool hitDefine      = false;



    // go through each line and turn it into tokens
    // during this, add each token to the tokens vector
    while(std::getline(inputFile, line)) {

        // If we hit a define, place a newline char in the output file
        // and in the terminal after we finish the line so that the next
        // tokens are placed one line down instead of next too it
        if (hitDefine) {
            std::cout << "\n";
            outputFile << "\n";
        }

        hitDefine   = false;

        if (line == "" || line == "\n") continue;

        for (std::string token : Utills::split(line, ' ')) {

            // check for single line comment
            if (token == "//") {
                // just move onto the next line, no special logic needed.
                break;
            }

            // checks if the multi-line comment is inside of a string
            if (token.find("/*") != std::string::npos && token.find("\"") == std::string::npos) {
                hitMulti = true;
                continue;
            }

            // must have already hit a multi-line comment beginning for the second to count
            if (token.find("*/") != std::string::npos && hitMulti) {
                hitMulti = false;
                continue;
            }

            // if we've hit a hash line token, we just want to write it to the output file
            if (hitHash) {
                if (verbose) Utills::out(" " + token, false, (hitDefine ? false : true), false);
                outputFile << token << (hitDefine ? " " : "\n"); // extra space incase its a #define
            }

            // skips #include and #define
            if (token.at(0) == '#') {
                if (verbose) Utills::out("Found: " + token, false, false);
                outputFile << token << "\n";
                continue;
            }

            // check if we have already made a mapping for this token
            if (Utills::findKeyInPairs(tokenCount, token)) {
                if (verbose) Utills::out("Found pre-existing token: " + token);

                // increment token count for that token
                tokenCount[Utills::indexOfPairByKey(tokenCount, token)].second++;

                tokens.push_back(token);
                continue;
            }

            // checks if were currently on a #include/define line
            // or in a multi-comment before creating a token map
            if (hitHash == false && hitMulti == false) {
                Utills::out(token, true);
                if (verbose) Utills::out("Found token: " + token);
                // add token to token count and start it at 1
                tokenCount.push_back(std::make_pair(token, 1));
                tokens.push_back(token);
            }
        }
        if (newLines && hitMulti != true) tokens.push_back("\n");
    }

    // sort token count.
    std::sort(tokenCount.begin(), tokenCount.end(),
        [](std::pair<std::string, int> a, std::pair<std::string, int> b) { return a.second > b.second; }
    );


    for (auto &p : tokenCount) {
        mapping[p.first] = currE;
        currE += "e";
    }

    // write each define
    for (auto &p : mapping) {
        outputFile << "#define " << mapping[p.first] << " " << p.first << "\n";
    }

    // write each tokens corresponding E from the mapping.
    for (auto &token : tokens) {
        outputFile << mapping[token] << " ";
    }


    inputFile.close();
    outputFile.close();
}

// epp inputFile <outputFile> [flags]
// flags: -n --new-lines -h --help -v --verbose
int main(int argc, char* argv[]) {
    // main will just parse the arguments and pass them along too the
    // parser, which will then take control of the program

    bool helpOnly = false;

    std::string inputFileName    = argv[1];
    std::string outputFileName   = argv[2];

    std::string helpString = "epp <inputFileName> [outputFileName] {flags}\nflags:\n\t-n --new-lines   Tells parser wether you want to keep code on one line or not.\n\t-h --help        Brings up the help page.\n\t-v --verbose     Tells parser wether to output what it's currently doing.";

    bool newLines   = false;
    bool verbose    = false;

    // evil code
    if (std::string(argv[1]) == "-h" || std::string(argv[1]) == "--help") {
        Utills::out(helpString);
        return 1;
    }


    if (argc < 2) {
        Utills::out("Must provide input file!", true);
        return 1;
    }

    if (inputFileName.substr(inputFileName.length() - 4, 4) != ".cpp") {
        Utills::out("Input file has to be a C++ file! (must end with: .cpp)", true);
        return 1;
    }

    // if we know that the first arg is -h
    // we then only need to read that one arg
    // so we start at it instead of the next one
    int i = helpOnly ? 1 : 2;

    for (; i < argc; i++) {
        std::string arg = argv[i];
        if (arg.at(0) == '=') {

            if (arg == "-n" || arg == "--new-lines") {
                newLines = true;
                continue;
            } else if (arg == "-h" || arg == "--help") {
                Utills::out(helpString);
                return 0;
            } else if (arg == "-v" || arg == "--verbose") {
                verbose = true;
                continue;
            } else {
                Utills::out("Invalid argument: " + arg, true);
                return 1;
            }

        }
    }

    parse(inputFileName, outputFileName, true, verbose);

}